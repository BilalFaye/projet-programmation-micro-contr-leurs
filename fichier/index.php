<html>
    <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    
    </head>
    <body>
		
    <div class="navbar"><span>Real-Time Chart with Plotly.js</span></div>
    <div class="wrapper">
        <div id="chart"></div>
        <?php
            $compteur = 0; //Variable qui donne la position actuelle
            $fichier = fopen("fichier.txt", 'a+');
            $donnees = array();
            $i = 0;
			while(!feof($fichier)){
				$ligne = fgets($fichier);
				if(!empty($ligne)){
					array_push($donnees, (int) $ligne);
				
					$i++;}
			}
			fclose($fichier);
			function incrementer_compteur(){
				$nombre = count($GLOBALS['donnees']);
				$nombre = $nombre - 1;
				if($nombre > $GLOBALS['compteur'])
					$GLOBALS['compteur'] = $GLOBALS['compteur'] + 1;
			}
			function element_courant(){ // Cette fonction permet de retouner la distance la plus récente enrégistrée
										// dans le fichier
				return  (string) $GLOBALS['donnees'][$GLOBALS['compteur']];
			}
				$str ="";
				for($i=0; $i< count($donnees); $i++){
					$str.=$donnees[$i];
					$str.=" ";
				}
        ?>
        <script>
			var a="<?php echo $str; ?>";
			var tab = a.split(" ");
			var taille = tab.length;
            function getData() {
                return Math.random();
            }  
            var compteur = 0;
            Plotly.plot('chart',[{
                y:[getData()],
                type:'line'
            }]);
            
            setInterval(function(){
				
                Plotly.extendTraces('chart',{y:[[ parseFloat(tab[compteur])]]}, [0]);
                if(compteur < taille);
					compteur++;
            },1000); // 1000 milli secondes 
            
        </script>
        
    </div>
    </body>
</html>



<?php
//require_once("./jpgraph/src/include_path_inc.php");

require_once("./jpgraph/src/jpgraph.php"); // toujours inclus quelque soit le type de graphe

require_once("./jpgraph/src/jpgraph_line.php"); // pour des courbes
require_once("./jpgraph/src/jpgraph_bar.php"); // inclus pour les histogrammes

/*$fichier = fopen("fichier.txt", 'a+');

$fichier = fopen("fichier.txt", 'a+');
$donnees = array();
while(!feof($fichier)){
	$ligne = fgets($fichier);
	array_push($donnees, (int) $ligne);
}
$largeur = 700;
$hauteur = 500;

// Initialisation du graphique
$graphe = new Graph($largeur, $hauteur);
// Echelle lineaire ('lin') en ordonnee et pas de valeur en abscisse ('text')
// Valeurs min et max seront determinees automatiquement
$graphe->setScale("textlin");
$graphe->SetMargin(40,130,20,40);
$graphe->SetShadow();

// Creation de l'histogramme
$histo = new BarPlot($donnees);
$histo->SetColor("green");
$histo->SetLegend("Histogramme");

//Création de courbes
$courbe =  new LinePlot($donnees);
$courbe->SetColor("red");
$courbe->SetWeight(5);
$courbe->SetLegend("Courbe");

// Ajout de l'histogramme au graphique
$graphe->add($histo);
$graphe->add($courbe);
// Ajout du titre du graphique
$graphe->title->set("Ultrason");

// Affichage du graphique
$graphe->stroke();
fclose($fichier);
/*
 * Vous auriez préféré une courbe plutôt qu'un histogramme? Facile, il suffit de
Remplacer l'inclusion de jpgraph_bar.php par jpgraph_line.php.
Remplacer l'instanciation de la classe BarPlot() par BarLine().
*/
 
?>
