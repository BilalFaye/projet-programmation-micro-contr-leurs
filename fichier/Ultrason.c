    void main(void) {
	  UART1_Init(4800);                         // initialize UART1 module
      Delay_ms(100);
      UART1_Write_Text("Start");
      UART1_Write(13);
      UART1_Write(10);
      	
      int compte = 0;
      float nombreaffiche = 0;
      ANSELB = 0x00;  //  toutes les entr�es sont numeriques
      TRISA = 0x00;   // Pour le LED A0->LED0, A1->LED1, ...
      TRISB = 0x01;   // B0: INPUT port de l'echo, B1: OUTPUT port du trigger
      TRISD = 0x00;
      LATD = 0x00;
      etape = 0;

      afficheNombre((int)nombreaffiche); // AFFICHAGE DE 0 SUR LED0
      while(1){
        switch(etape) {

         case 0:
         
         LATB1_bit = 1; // envoie d'une impulsion d'horloge par le trigger
         delay_us(10); // On maintient l'impulsion pendant 10 micro secondes
         LATB1_bit = 0;
         compte = 0;
         etape++ ;
         break;
         
         case 1:
			if(PORTB & 0x01){         // si le trigger envoie une impulsion, l'echo sera activ� tant que l'onde n'est pas r�ceptionn�.
				while(PORTB & 0x01){ // on compte le temps d'attente de l'onde que l'on convertit en micro seconde avec delay_us()
					compte++;
                    delay_us(10);
                }
                etape++;
            }
			break;

         case 2:
			nombreaffiche = (int)compte*10/58.0;  // sachant la vitesse de l'onde qui est de 340m/s et le temps d'attente
                                                 //(compte en micro secondes ), on calcule 
            UART1_Write(nombreaffiche);  // la distance par rapport � l'obstacle
			Delay_ms(1);
			etape=0;   
			break;
        }
      }
    }
